package fr.epsi.b3.productsapi.web.controllers;

import fr.epsi.b3.productsapi.business.entities.Product;
import fr.epsi.b3.productsapi.web.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/products")
public class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping(value = "/", produces = "application/json")
    public ResponseEntity<List<Product>> getAllBom() {
        List<Product> products = productService.getAll();
        return ResponseEntity.ok(products);
    }
}
