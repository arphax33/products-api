package fr.epsi.b3.productsapi.web.dtos;

import fr.epsi.b3.productsapi.business.enums.ProductStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    private String name;
    private String reference;
    private ProductStatus status;
}
