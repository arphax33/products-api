package fr.epsi.b3.productsapi.web.mappers;

import fr.epsi.b3.productsapi.business.entities.Product;
import fr.epsi.b3.productsapi.web.dtos.ProductDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    ProductDto productToProductDto(Product product);
    Product productDtoToProduct(ProductDto product);
    List<ProductDto> productsToProductDtos(List<Product> products);
    List<Product> productToProductDto(List<ProductDto> productDtos);
}
