package fr.epsi.b3.productsapi.web.services;

import fr.epsi.b3.productsapi.business.entities.Product;
import fr.epsi.b3.productsapi.business.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    public List<Product> getAll() {
        return productRepository.findAll();
    }
}
