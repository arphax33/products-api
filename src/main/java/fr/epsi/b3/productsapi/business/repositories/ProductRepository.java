package fr.epsi.b3.productsapi.business.repositories;

import fr.epsi.b3.productsapi.business.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface ProductRepository extends JpaRepository<Product, Integer> {
}
