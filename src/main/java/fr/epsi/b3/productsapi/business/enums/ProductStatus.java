package fr.epsi.b3.productsapi.business.enums;

public enum ProductStatus {
    AVAILABLE("disponible"),
    PENDING("reaprovisionnement"),
    NOSTOCK("epuise");

    private String value;

    private ProductStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
