CREATE TABLE product
(
    id bigint IDENTITY NOT NULL,
    name VARCHAR (50) NOT NULL,
    reference VARCHAR (15) NOT NULL,
    status VARCHAR (50) NOT NULL
);

ALTER TABLE product
    ADD
        constraint product_pk PRIMARY KEY CLUSTERED (id) WITH (
    ALLOW_PAGE_LOCKS = ON,
    ALLOW_ROW_LOCKS = ON
    );